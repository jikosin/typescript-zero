## Building

Run `make` to build a version of the compiler/language service that reflects changes you've made.
You can then run `node <repo-root>/built/local/tsc.js` in place of `tsc` in your project. For example, to run `tsc --watch` from within the root of the repository on a file called `test.ts`, you can run `node ./built/local/tsc.js --watch test.ts`.

## Testing:
### Single File Test
`node built/local/tsc.js file.ts`

### Project Test
`node built/local/tsc.js --build path/to/tsconfig.json`

